<?php
/**
 * Plugin Name: 	Endeavr / ChurchAmp - Dashboard
 * Plugin URI: 	http://churchamp.com/plugins/dashboard
 * Description: 	A ChurchAmp Feature Module. [Dashboard]
 * Version: 		1.0.0
 * Author: 		Endeavr Media (Jason Loftis / jLoft)
 * Author URI: 	http://endeavr.com
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @package		ChurchAmp_Dashboard
 * @version		1.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/dashboard
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

define( 'ENDVR_CA_DASHBOARD_URL', plugin_dir_url(__FILE__) );

// Add Dashboard Widgets
include_once('widgets/churchamp-dashboard-widgets.php');