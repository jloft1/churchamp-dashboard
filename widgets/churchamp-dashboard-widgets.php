<?php
/**
 * @package		ChurchAmp_Dashboard
 * @subpackage		widgets
 * @version		1.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/dashboard
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

function endvr_churchamp_dashboard_widget_one() { ?>

	<img src="<?php echo ENDVR_CA_DASHBOARD_URL . 'images/churchamp.png'; ?>" width="auto" height="auto">
	<div class="dashblock">
		<h4 class="sub">ChurchAmp Intro - Sermon Publishing</h4><br>
		<?php echo do_shortcode('[video]http://youtu.be/U-RgIQYkJUc[/video]'); ?>
	</div>

<?php
}

add_action( 'wp_dashboard_setup', 'endvr_churchamp_dashboard_setup_function' );
function endvr_churchamp_dashboard_setup_function() {
    add_meta_box( 'endvr_churchamp_dashboard_widget_one', 'ChurchAmp Tutorials', 'endvr_churchamp_dashboard_widget_one', 'dashboard', 'normal', 'high' );
}